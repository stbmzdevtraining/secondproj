package mz.co.stdbank.second;

import mz.co.stdbank.second.persistence.PersonDAO;
import mz.co.stdbank.second.persistence.PersonEntity;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Optional;

@Path("/people")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class PeopleAPI {

    private static Person person;

    @Inject
    private PersonDAO dao;

    static {
        person = new Person();
        person.setId("a231152");
        person.setName("Mario");
        person.setEmail("mario.junior@standardbank.co.mz");
    }


    @GET
    @Path("/{personId}")
    public Response findPersonById(@PathParam("personId") int id){
        Optional<PersonEntity> entityOptional = dao.findById(id);
        if(!entityOptional.isPresent()){
            return Response.status(404).build();
        }

        PersonEntity entity = entityOptional.get();
        Person person  = new Person();
        person.setName(entity.getName());
        person.setEmail(entity.getEmail());
        person.setId(String.valueOf(entity.getId()));
        return Response.ok(person).build();

    }

    /*
    public Response findPersonByIdOld(@PathParam("personId") String id){
        if(!person.getId().equals(id))
            return Response.status(404).build();
        return Response.ok(person).build();
    }*/

    @POST
    @Path("/{personId}/name")
    //@Consumes(MediaType.TEXT_PLAIN)
    public Response changeName(@PathParam("personId") String id, String name){
        if(!person.getId().equals(id))
            return Response.status(404).build();
        person.setName(name);
        return Response.ok().build();
    }

    @POST
    @Path("/create")
    @Produces(MediaType.TEXT_PLAIN)
    public Response create(Person person){

        PersonEntity entity = new PersonEntity();
        entity.setName(person.getName());
        entity.setEmail(person.getEmail());
        dao.save(entity);
        return Response.ok(String.valueOf(entity.getId())).build();

    }

}
