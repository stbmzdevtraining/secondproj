package mz.co.stdbank.second.persistence;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public final class Database {

    public static final EntityManagerFactory ENTITY_MANAGER_FACTORY = Persistence
            .createEntityManagerFactory(
                    "TestPersistence");

}
