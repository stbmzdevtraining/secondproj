package mz.co.stdbank.second.persistence;

import java.util.List;
import java.util.Optional;

public interface PersonDAO {
    Optional<PersonEntity> findById(int id);

    void delete(PersonEntity personEntity);

    void deleteById(int id);

    List<PersonEntity> list();

    void save(PersonEntity personEntity);
}
