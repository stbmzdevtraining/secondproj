package mz.co.stdbank.second.persistence;

import javax.persistence.*;
import java.util.List;
import java.util.Optional;

public class PersonDAOImpl implements PersonDAO {

    private EntityManager getEntityManager(){

        return Database.ENTITY_MANAGER_FACTORY
                .createEntityManager();

    }

    public void save(PersonEntity personEntity){
        EntityManager em = getEntityManager();
        try {
            EntityTransaction transaction = em.getTransaction();
            transaction.begin();
            em.persist(personEntity);
            transaction.commit();
        }finally {
            em.close();
        }
    }

    @Override
    public Optional<PersonEntity> findById(int id){
        EntityManager em = getEntityManager();
        try{

            TypedQuery<PersonEntity> q = em.
                    createQuery("select p from PersonEntity p where p.id=:id",
                    PersonEntity.class);
            q.setParameter("id",id);
            PersonEntity result = q.getSingleResult();
            System.out.println("Result found");
            return Optional.of(result);

        }catch (NoResultException ex){
            System.out.println("No Person record with Id="+id);
            ex.printStackTrace();
            return Optional.empty();
        }finally {
            em.close();
        }


    }

    @Override
    public void delete(PersonEntity personEntity){

        //TODO: Implement
        throw new UnsupportedOperationException();

    }

    @Override
    public void deleteById(int id){

        //TODO: Implement
        throw new UnsupportedOperationException();

    }

    @Override
    public List<PersonEntity> list(){

        //TODO: Implement
        throw new UnsupportedOperationException();

    }



}
